// LOAD CALENDAR
// Verify if cache is empty
if (jsonString == null) {
    jsonString = '';
} else {
    // If is not empty, load table
    let classes = JSON.parse(jsonString);
    for (var i in classes) {
        const lessonName = classes[i]['lesson'];
        const periods = classes[i]['periods'];
        const siteURL = classes[i]['url'];
        for (j in periods) {
            const rowId = `${periods[j].slice(1)}_Row`;
            const day = dayOfTheWeekIndex(periods[j]);
            var cell = document.getElementById(rowId).cells[day + 1];
            let classLink = createLink(lessonName, siteURL);
            cell.appendChild(classLink);
        }
    }
}

// Change column color if isn't today.
for (let i = 1; i < 8; i++) {
    for (let j = 1; j < 8; j++) {
        if (weekDay != i) {
            table.rows[j].cells[i].style.borderColor = 'LightGray';
        }
    }
}

if (focusString == null || focusString == 'y') {
    focusButton.classList.add('buttonIsSelected');
    focusButtonIsSelected = true;
    var cell = updateClassBackground();
    try {
        document.getElementById(`${now.now}_Row`).cells[weekDay];
        cell.title = 'Current class';
    } catch (error) {
        try {
            document.getElementById(`${now.next}_Row`).cells[weekDay];
            cell.title = 'Next class';
        } catch (error) { }
    }
}

if (autoOpenerString != null && autoOpenerString == 'y') {
    autoButton.classList.add('buttonIsSelected');
    autoButtonIsSelected = true;
    openURL();
}